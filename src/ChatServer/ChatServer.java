package ChatServer;

import java.awt.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;



public class ChatServer implements IChatServer {
	

  public ArrayList<AbstractRoom> rooms = new ArrayList<AbstractRoom>();
  public ArrayList<ChatMessage> sendedMessages = new ArrayList<ChatMessage>(); // messages
  public static ArrayList<AbstractUser> user = new ArrayList<AbstractUser>(); //clients
  public static ArrayList<String> LoggedNames = new ArrayList<String>();
  public static ArrayList<Integer> LoggedIDs = new ArrayList<Integer>();
  public ArrayList<GeneralUser> roomuser = new ArrayList<GeneralUser>();  
  private static  ChatServer  instance;
 
  // public ChatServer chatServer;
  
  ChatServer()
  {
     System.out.println("ChatServer(): Initializing Instance");
  }
  public synchronized static ChatServer getInstance()
  {
     if (instance == null)
        instance = new ChatServer();
     return instance;
  }
  
  //----------------------------------------------


  public void  addUser(GeneralUser user1) 
  {
	  //main
	  Scanner scan = new Scanner (System.in);
	  int choose;
	 
	  int id= scan.nextInt();
	  user1 = new GeneralUser();
	  user1.setID(id);
      Scanner scan1 =  new Scanner (System.in);
	  String name = scan1.nextLine();
	  user1.setName(name);
	   System.out.println(user1.getID());
      System.out.println(user1.getName());
      
	  
      PersistanceFActory persistanceFactory = new PersistanceFActory();
 	  IPersistanceMechanism file =  PersistanceFActory.loadPersistanceMechanism(1); 
      file.addUser(user1.getID(), user1);
	  user.add(user1);
	  
	  for(int i=0;i<user.size();i++)
	  {
		  if(user1.getID() == user.get(i).getID())
		  {
			  System.out.println("the user is already exists");
			  //return false;
			  break;
		  }
		  
		  
	  }
	  
       file = new FilePersistance();
      file.addUser(user1.getID(), user1);
	  user.add(user1);
	 // return true;

	  
  }

  public void addRoomAdmin(AdminUser admin, ArrayList<String> permissions) 
  {
	 /* main
	  Scanner scan = new Scanner (System.in);
	  int choose;
	 
	  int id= scan.nextInt();
	  
	  admin.setID(id);
      Scanner scan1 =  new Scanner (System.in);
	  String name = scan1.nextLine();
	  admin.setName(name);
	  
	   System.out.println(admin.getID());
       System.out.println(admin.getName());
       System.out.println("List the permission of the Admin");
       
       for(int i = 0 ; i < permissions.size();i++)
    	 admin.addPermission(scan.next());
    	 */
	  for(int i =0;i<user.size();i++)
	  {
		  if(admin.getID()==user.get(i).getID())
		  {
			  PersistanceFActory persistanceFactory = new PersistanceFActory();
		 	  IPersistanceMechanism file =  PersistanceFActory.loadPersistanceMechanism(1); 
		      file.addUser(admin.getID(), admin);
			  user.add(admin);
		  }
	  }
	  
	  System.out.println("the user is not already exists");
      
  }

  public void removeUser(int userID) 
  {
	  FileReader fr;
	try {
		fr = new FileReader("User.txt");
	
      BufferedReader br = new BufferedReader(fr);
      PersistanceFActory persistanceFactory = new PersistanceFActory();
 	 IPersistanceMechanism file =  PersistanceFActory.loadPersistanceMechanism(1); 
      String str;
      String s;
      System.out.println("shit");
      while( ( s = br.readLine() )!= null ){
          
    	  ChatServer users = new ChatServer(); 
    	  GeneralUser ob = new GeneralUser ();
      int a;
      for( a = 0; a < s.length(); a++) 
      {
          if( s.charAt(a) != ' ')
              break;
      }
      
      str = s.substring(a);
      String[] splitStr = str.split("\\s+"); 
      
        ob.setID(Integer.parseInt(splitStr[0]));
        //users.UserPassword = splitStr[1];
	 ob.setName(splitStr[1]);
	 user.add(ob);
	
 }
      System.out.println("AAA");
      file.deleteUser(userID);
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (NumberFormatException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
	  
  }
  public void login (){
	  Scanner scan = new Scanner (System.in);
	  System.out.println("..............Login Page...................");
	  FileReader fr;
		try {
			fr = new FileReader("User.txt");
		
	      BufferedReader br = new BufferedReader(fr);
	      PersistanceFActory persistanceFactory = new PersistanceFActory();
	 	 IPersistanceMechanism file =  PersistanceFActory.loadPersistanceMechanism(1); 
	      String str;
	      String s;
	     
	      while( ( s = br.readLine() )!= null ){
	          
	    	  ChatServer users = new ChatServer(); 
	    	  GeneralUser ob = new GeneralUser ();
	      int a;
	      for( a = 0; a < s.length(); a++) 
	      {
	          if( s.charAt(a) != ' ')
	              break;
	      }
	      
	      str = s.substring(a);
	      String[] splitStr = str.split("\\s+"); 
	      
	        ob.setID(Integer.parseInt(splitStr[0]));
	        //users.UserPassword = splitStr[1];
		 ob.setName(splitStr[1]);
		 user.add(ob);}
	      Scanner scan1 = new Scanner (System.in);
	    System.out.println("UserName: ");
	    String name= scan.nextLine();
	    System.out.println("Password: ");
	    int id= scan1.nextInt();
	    FileWriter write = new FileWriter("loggedin.txt");
	    BufferedWriter bw = new BufferedWriter(write);
	    boolean t = false;
	 for(int i=0; i<user.size();i++){
		 if(name.equals(user.get(i).name)&& id== user.get(i).id){
			 System.out.println("......U Are Logged in......");
			 LoggedNames.add(name);
			 LoggedIDs.add(id);
			 t =  true;
			
			break;
		 }
	 }
	 write.write(LoggedNames.get(0));
	 
		write.write(" ");
		write.write(Integer.toString(LoggedIDs.get(0)));
		write.write("\n");
		write.close();
	    if(t==false){
	    	System.out.println("there is no such username and password");
	    }
	      } catch (FileNotFoundException e) {
	  		// TODO Auto-generated catch block
	  		e.printStackTrace();
	  	} catch (NumberFormatException e) {
	  		// TODO Auto-generated catch block
	  		e.printStackTrace();
	  	} catch (IOException e) {
	  		// TODO Auto-generated catch block
	  		e.printStackTrace();
	  	}
	  
  }

  public void removeAllUser(int roomID) 
  {
	  FileWriter writer ;
	  try {
		writer = new FileWriter("User.txt");
		 
		writer.write("");
		  writer.close();
			 
		} 
		  catch (IOException e)	{e.printStackTrace();}
  }

  public void removeRoom(int roomID) 
  {
	  
	         System.out.println(roomID); 
	  FileReader fr;
		try {
			fr = new FileReader("room.txt");
		
	      BufferedReader br = new BufferedReader(fr);
	      FilePersistance file = new FilePersistance();
	      String str;
	      String s;
	      System.out.println(" before while  ");
	      while( ( s = br.readLine() )!= null ){
	          
	    	  ChatServer users = new ChatServer(); 
	    	  GeneralUser ob = new GeneralUser ();
	      int a;
	      for( a = 0; a < s.length(); a++) 
	      {
	          if( s.charAt(a) != ' ')
	              break;
	      }
	      str = s.substring(a);
	      String[] splitStr = str.split("\\s+"); 
	      ob.setID(Integer.parseInt(splitStr[0]));
	        //users.UserPassword = splitStr[1];
		 ob.setName(splitStr[1]);
		 user.add(ob);
		
	 }
	      System.out.println(" barr el while");
	      file.deleteRoom (roomID);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	      
	  
	  
  }


  public ArrayList getRooms() {
  return rooms;
  }

  public void joinRoom(int roomID, int userID) 
  {
	  Boolean isUser = false;
	  Boolean isRoom = false;
	  AbstractUser newUser = new GeneralUser();
	  int i = 0 , j = 0;
	  for( i = 0 ; i < user.size() ; i++) // if the user is in this chat server
	  {
		  if ( userID == user.get(i).getID())
		  {
			  isUser = true;
			  newUser.setID(user.get(i).getID()); 
			  break;
		  }
	  }
	  for( j = 0 ; j < rooms.size() ; j++) //check the roomID is in this chatServer or not
	  {
		  if ( roomID == rooms.get(j).getID())
		  {
			  isRoom = true;
			  break;
		  }
	  }
	  if( isRoom && isUser)
	  {
		  rooms.get(j).users.add(newUser);
	  }
	  
  }

  public void removeAllRooms(ArrayList<AbstractRoom> rooms) 
  {
	 FileWriter writer ;
	  try {
		writer = new FileWriter("room.txt");
		 
		writer.write("");
		  writer.close();
		 
	} 
	  catch (IOException e)	{e.printStackTrace();}
	  
  }

  public void creatRestirctedRoom() {
	
	  System.out.println("RestrictedRoom");
	  String filename= "Restricted";
	  System.out.println("Enter ID");
	  Scanner scan = new Scanner (System.in);
	  int id= scan.nextInt();
	 
	  System.out.println("Enter title");
      Scanner scan1 =  new Scanner (System.in);
	  String name = scan1.nextLine();
	  System.out.println("Enter description");
	  Scanner scan2 = new Scanner (System.in);
	  String desc = scan2.nextLine();
	  RestrictedRoom restrict = new RestrictedRoom(name, desc,roomuser);
	  restrict.setID(id);
	 
	 PersistanceFActory persistanceFactory = new PersistanceFActory();
	 IPersistanceMechanism file =  PersistanceFActory.loadPersistanceMechanism(1); 
      file.addRoom(restrict.getID(), restrict, filename);
	  rooms.add(restrict);
  }

  public void createGenralRoom() {
	
	  // System.out.println("....."+LoggedNames.get(0)+"............");
	 String filename ="room";
	 System.out.println("Enter id");
	  Scanner scan = new Scanner (System.in);
	  int id= scan.nextInt();

	//  GeneralUser ob = new GeneralUser ();
	 
	  System.out.println("Enter title");
      Scanner scan1 =  new Scanner (System.in);
	  String name = scan1.nextLine();
	  
	  // System.out.println(user1.getID());
     //System.out.println(user1.getName());
	  System.out.println("Enter description");
	 Scanner scan2 = new Scanner (System.in);
	 String desc = scan2.nextLine();
	 GeneralRoom general = new GeneralRoom(name, desc);
	  general.setID(id);
	 general.setTitle(name);
	 general.setDecription(desc);
	 PersistanceFActory persistanceFactory = new PersistanceFActory();
	 IPersistanceMechanism file =  PersistanceFActory.loadPersistanceMechanism(1); 
      file.addRoom(general.getID(), general,filename);
	  rooms.add(general);
	
  }

  public void sendMessage(ChatMessage message)
  {
	  sendedMessages.add(message);
  }

  public void leaveRoom(int roomID, int userID)
  {
	  Boolean isUser = false;
	  Boolean isRoom = false;
	  AbstractUser newUser = new GeneralUser();
	  int i = 0 , j = 0;
	  for( i = 0 ; i < user.size() ; i++) // if the user is in this chat server
	  {
		  if ( userID == user.get(i).getID())
		  {
			  isUser = true;
			  break;
		  }
	  }
	  for( j = 0 ; j < rooms.size() ; j++) //check the roomID is in this chatServer or not
	  {
		  if ( roomID == rooms.get(j).getID())
		  {
			  isRoom = true;
			  break;
		  }
	  }
	  if( isRoom && isUser)
	  {
		  rooms.get(j).users.remove(i);
	  }
	  
  }



  public void loadPersistanceConfigurations() 
  {
	  
	
	  GeneralUser user1 = new GeneralUser();
	  user1.setID(3);
     
	//  String name = scan1.nextLine();
	  user1.setName("eman");
	  
      PersistanceFActory persistanceFactory = new PersistanceFActory();
 	  IPersistanceMechanism file =  PersistanceFActory.loadPersistanceMechanism(1); 
      file.addUser(user1.getID(), user1);
	  user.add(user1);
	  String filename ="room";
	  GeneralRoom general = new GeneralRoom ("general" , "New Room");
	  general.setID(1);
	
	  file.addRoom(general.getID(), general,filename);
	  rooms.add(general);
	  
  }

  public void blockUser(int userID, int blockedUserID) 
  {
	  GeneralUser ob = new GeneralUser();
	  System.out.println("please enter your ID");
	  Scanner scan = new Scanner(System.in);
	  userID= scan.nextInt();
	  for(int i=0;i<user.size();i++){
		  if(userID == user.get(i).id){
			  System.out.println("please enter the id of the user you want to block");
			  blockedUserID=scan.nextInt();
			  ob.addBlockedUser(userID, blockedUserID);
		  }
	  }
  }




}