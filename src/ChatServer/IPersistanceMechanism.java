package ChatServer;

//import ChatServer.int;
//import ChatServer.List<AbstractUser>;
import java.awt.List;
import java.util.ArrayList;

import ChatServer.IPersistanceMechanism;
import ChatServer.AbstractRoom;
import ChatServer.AbstractUser;

public interface IPersistanceMechanism {

  public void addUser(int userID, AbstractUser user);

  public AbstractUser getUser();

  public void deleteUser(int userID);

  public void addRoom(int userID, AbstractRoom room, String filename);

  public AbstractRoom getRoom();

  public void deleteRoom(int roomID);

  public ArrayList<AbstractRoom> getAllRooms();

  public ArrayList<AbstractUser> getAllUsers(int roomID);

  public IPersistanceMechanism getInstance();

}