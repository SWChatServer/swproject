package ChatServer;

import java.awt.List;
import java.io.FileNotFoundException;
import java.util.ArrayList;

//import String;

public interface IChatServer {

 public void addUser(GeneralUser user);

 public	void addRoomAdmin(AdminUser admin, ArrayList<String> permissions);

 public	void removeUser(int userID) throws FileNotFoundException;

 public	void removeAllUser(int roomID);

 public	void removeRoom(int roomID);

 public ArrayList getRooms();

 public void joinRoom(int roomID, int userID);

 public void removeAllRooms(ArrayList<AbstractRoom> rooms);

 

 public void createGenralRoom();

 public void sendMessage(ChatMessage message);

 public void leaveRoom(int roomID, int userID);


  public void loadPersistanceConfigurations();

  public void blockUser(int userID, int blockedUserID);

}