package ChatServer;

public class PersistanceFActory {

  public static  IPersistanceMechanism loadPersistanceMechanism(int roomType) 
  {
	   if(roomType == 1)
	   {
		  return new FilePersistance();
	   }
	   else
	   if(roomType == 2)
	   {
		   return new SQLPersistance();
	   }
	   return new FilePersistance();
  }

}