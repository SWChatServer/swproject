package ChatServer;

import java.awt.List;
import java.util.ArrayList;

//import String;

public class RoomMessage extends ChatMessage {

  public ArrayList<AbstractRoom> roomList= new ArrayList<AbstractRoom>();

  public  RoomMessage(AbstractUser sender, AbstractRoom room, String message)
  {
	  send = new AbstractUser();
	  send.setName(sender.getName()); 
	  send.setID(sender.getID());
	  roomList.add(room);
	  this.setMessage(message);
  }

  public  RoomMessage(AbstractUser sender, ArrayList<AbstractRoom> roomList, String message)
  {
	 send = new AbstractUser();
      send.setName(sender.getName());  
	  send.setID(sender.getID());
	  for (int i = 0 ; i < roomList.size() ; i++)
	  {
		this.roomList.set(i , roomList.get(i));
	  }
	  this.setMessage(message);
			
  }

}