package ChatServer;

import java.awt.List;
import java.util.ArrayList;



//import String;

public class AbstractRoom {

  public String title;

  public String desciption;

  public ArrayList <AbstractUser>users;

  public int id;

  public AbstractRoom()
  {
	  title = "";
	  desciption = "";
	  
  }
  public AbstractRoom(String title , String desc ,ArrayList <AbstractUser>users ,int id  )
  {
	this.title = title;
	 desciption = desc;
	 this.users = users;
	 this.id = id;
  }
  public void setTitle(String title) 
  {
	 this.title = title;
  }

  public String getTitle() {
  return title;
  }

  public void setDecription(String desc)
  {
	  desciption = desc;
  }

  public String getDesciption() {
  return desciption;
  }

  public ArrayList<AbstractUser> getUsers() {
  return users;
  }

  public void setID(int roomId) 
  {
	  id = roomId;
  }

  public int getID() {
  return id;
  }

}