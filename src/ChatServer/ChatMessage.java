package ChatServer;

//import String;

public class ChatMessage {

  public AbstractUser send;

  private String message;

  public void setSender(AbstractUser user) 
  {
	  send = user;
  }

  public AbstractUser getSender() {
  return send;
  }

  public void setMessage(String msg) 
  {
	  message = msg;
  }

  public String getMessage() {
	  return message;
  }



}