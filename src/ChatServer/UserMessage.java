package ChatServer;
import java.util.ArrayList;
public class UserMessage extends ChatMessage 
{
	public ArrayList<AbstractUser> recipients = new ArrayList<AbstractUser>(); 
	
	public UserMessage(AbstractUser sender, AbstractUser receiver, String message)
	{ 
		send = new AbstractUser();
		send.setName(sender.getName()); 
		send.setID(sender.getID());
		recipients.add(receiver);
		this.setMessage(message);	
	}
	public  UserMessage(AbstractUser sender, ArrayList<AbstractUser> recipientsList, String message)
	{
	 send = new AbstractUser();
	 send.setName(sender.getName());  
	 send.setID(sender.getID());
	 for (int i = 0 ; i < recipientsList.size() ; i++)
	 {
		 recipients.set(i , recipientsList.get(i));
	 }
	 this.setMessage(message);
		
	} 
}